/* crear o cambirme a una bd use */ 

use instituto

// crear una coleccion (tabla)
//Borrar bd db . dropDatabase ()

db.createCollection("profesores")
db.createCollection("estudiantes")
db.createCollection("cursos")

// crear documentos (registros)


db.profesores.insertMany([
    {
    	_id :  new ObjectId("5fb042144f50bdc3ac48d18f"),
        nombre:'Oscar',
        apellido: 'Palacios',
        correo:'oscar@gmail.com',
        documento:'1000170434'
    },
    {
    	_id :  new ObjectId("5fb042144f50bdc3ac48d190"),
         nombre:'Christian',
        apellido: 'Ruiz',
        correo:'christian@gmail.com',
         documento:'1000170435'
    },
    {
    	_id :  new ObjectId("5fb042144f50bdc3ac48d191"),
         nombre:'Manuel',
        apellido: 'Moncada',
        correo:'moncada@gmail.com',
         documento:'1000170433'
    },
     {
     	_id :  new ObjectId("5fb042144f50bdc3ac48d192"),
         nombre:'William',
        apellido: 'Ochoa',
        correo:'william@gmail.com',
          documento:'1000170432'
    },
     {
     	_id :  new ObjectId("5fb042144f50bdc3ac48d193"),
         nombre:'Rishard',
        apellido: 'David',
        correo:'rishard@gmail.com',
          documento:'1000170431'
    }
])

    
db.estudiantes.insertMany([
    {
    	_id :  new ObjectId("5fb042314f50bdc3ac48d194"),
        nombre:'Oscar',
        apellido: 'Gomez',
        correo:'OG@gmail.com',
        documento:'1000170481'
    },
    {
    	_id :  new ObjectId("5fb042314f50bdc3ac48d195"),
         nombre:'Christian',
        apellido: 'Sanchez',
        correo:'CS@gmail.com',
         documento:'1000170482'
    },
    {
    	_id :  new ObjectId("5fb042314f50bdc3ac48d196"),
         nombre:'Manuel',
        apellido: 'Quiroz',
        correo:'MQ@gmail.com',
         documento:'1000170483'
    },
     {
     	_id :  new ObjectId("5fb042314f50bdc3ac48d197"),
         nombre:'William',
        apellido: 'Quintero',
        correo:'WQ@gmail.com',
          documento:'1000170484'
    },
     {
     	_id :  new ObjectId("5fb042314f50bdc3ac48d198"),
         nombre:'Rishard',
        apellido: 'S',
        correo:'RS@gmail.com',
          documento:'1000170485'
    },
        {
        _id :  new ObjectId("5fb042314f50bdc3ac48d199"),
        nombre:'Oscar',
        apellido: 'Hincapie',
        correo:'OH@gmail.com',
        documento:'1000170486'
    },
    {
    	_id :  new ObjectId("5fb042314f50bdc3ac48d19a"),
         nombre:'Cristian',
        apellido: 'David',
        correo:'CD@gmail.com',
         documento:'1000170487'
    },
    {
    	_id :  new ObjectId("5fb042314f50bdc3ac48d19b"),
         nombre:'Manuel',
        apellido: 'Esteban',
        correo:'ME@gmail.com',
         documento:'1000170488'
    },
     {
     	_id :  new ObjectId("5fb042314f50bdc3ac48d19c"),
         nombre:'William',
        apellido: 'David',
        correo:'WD@gmail.com',
          documento:'1000170489'
    },
     {
     	_id :  new ObjectId("5fb042314f50bdc3ac48d19d"),
         nombre:'Rishard',
        apellido: 'Gomez',
        correo:'RG@gmail.com',
          documento:'1000170490'
    },
        {
        _id :  new ObjectId("5fb042314f50bdc3ac48d19e"),
        nombre:'Bryan',
        apellido: 'David',
        correo:'BD@gmail.com',
        documento:'1000170491'
    },
    {
    	_id :  new ObjectId("5fb042314f50bdc3ac48d19f"),
         nombre:'Oscar',
        apellido: 'Quintero',
        correo:'OQ@gmail.com',
         documento:'1000170492'
    },
    {
    	_id :  new ObjectId("5fb042314f50bdc3ac48d1a0"),
         nombre:'Daniel',
        apellido: 'Tuzzo',
        correo:'DT@gmail.com',
         documento:'1000170493'
    },
     {
     	_id :  new ObjectId("5fb042314f50bdc3ac48d1a1"),
         nombre:'Carolina',
        apellido: 'Ochoa',
        correo:'CO@gmail.com',
          documento:'1000170494'
    },
     {
     	_id :  new ObjectId("5fb042314f50bdc3ac48d1a2"),
         nombre:'Amparo',
        apellido: 'David',
        correo:'AD@gmail.com',
          documento:'1000170495'
    },
        {
        _id :  new ObjectId("5fb042314f50bdc3ac48d1a3"),
        nombre:'Andrea',
        apellido: 'Palacios',
        correo:'AP@gmail.com',
        documento:'1000170496'
    },
    {
    	_id :  new ObjectId("5fb042314f50bdc3ac48d1a4"),
         nombre:'Andres',
        apellido: 'Ruiz',
        correo:'AR@gmail.com',
         documento:'1000170497'
    },
    {
    	_id :  new ObjectId("5fb042314f50bdc3ac48d1a5"),
         nombre:'Vladimir',
        apellido: 'Moncada',
        correo:'VM@gmail.com',
         documento:'1000170498'
    },
     {
     	_id :  new ObjectId("5fb042314f50bdc3ac48d1a6"),
         nombre:'Marta',
        apellido: 'Ochoa',
        correo:'MO5@gmail.com',
          documento:'1000170499'
    },
     {
     	_id :  new ObjectId("5fb042314f50bdc3ac48d1a7"),
         nombre:'Luis',
        apellido: 'David',
        correo:'LD@gmail.com',
          documento:'1000170999'
    }
])


db.cursos.insertMany([
    {
    	_id :  new ObjectId("5fb042ca4f50bdc3ac48d1a8"),
        nombre:'PHP',
        creditos: '20'
       
    },
    {
    	_id :  new ObjectId("5fb042ca4f50bdc3ac48d1a9"),
         nombre:'JS',
        creditos: '20'
        
    },
    {
    	_id :  new ObjectId("5fb042ca4f50bdc3ac48d1aa"),
         nombre:'Testing',
        creditos: '10'
           },
        
     {
     	_id :  new ObjectId("5fb042ca4f50bdc3ac48d1ab"),
         nombre:'Diseño web',
        creditos: '10'
       
    },
     {
     	_id :  new ObjectId("5fb042ca4f50bdc3ac48d1ac"),
         nombre:'Java',
        creditos: '10'
       
    },
     {
     	_id :  new ObjectId("5fb042ca4f50bdc3ac48d1ad"),
         nombre:'C#',
        creditos: '10'
       
    },
     {
     	_id :  new ObjectId("5fb042ca4f50bdc3ac48d1ae"),
         nombre:'Python',
        creditos: '10'
       
    },
     {
     	_id :  new ObjectId("5fb042ca4f50bdc3ac48d1af"),
         nombre:'Ruby',
        creditos: '10'
       
    }
])

db.profesores.find().pretty()
db.estudiantes.find().pretty()
db.cursos.find().pretty()
// show collections lista las colecciones 

//  db.usuarios.find()  lista todos los docuememntos de una colleccion

// db.profesoresCurso.drop()   elimina la coleccion

// db.usuarios.find({"documento":"3456789"})

// db.usuarios.find({"documento":"3456789", "correo":"juanp@sena.edu.co"})

// db.usuarios.remove({"documento":"3456789"})

//  db.usuarios.find().pretty()


db.createCollection("profesoresCurso")

db.profesoresCurso.insertMany([
    {
        profesor: new ObjectId("5fb042144f50bdc3ac48d18f"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1a8")
        //cursos: 'PHP'
    
    },
    {
        profesor: new ObjectId("5fb042144f50bdc3ac48d190"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1a9")
          //cursos: 'JS'
    },
    {
          profesor: new ObjectId("5fb042144f50bdc3ac48d191"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1aa")
          //cursos: 'Testing'
    },
        {
          profesor: new ObjectId("5fb042144f50bdc3ac48d192"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1ab")
          //cursos: 'Diseño web'
    },
        {
          profesor: new ObjectId("5fb042144f50bdc3ac48d192"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1ac")
          //cursos: 'Java'
    },
        {
          profesor: new ObjectId("5fb042144f50bdc3ac48d193"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1ad")
          //cursos: 'C#'
    },
        {
          profesor: new ObjectId("5fb042144f50bdc3ac48d193"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1ae")
          //cursos: 'Python'
    },
        {
          profesor: new ObjectId("5fb042144f50bdc3ac48d190"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1af")
          //cursos: 'Ruby'
    },
])

db.profesoresCurso.find().pretty()

    // como hacer consultas de varias colecciones 

    db.profesores.aggregate(
        [
            {
                $match:{
                	//Poner id generada
                    _id: ObjectId("5fb042144f50bdc3ac48d190")
                }
            },
            {
                $lookup:{
                      from:'profesoresCurso',
                    localField:'_id',
                    foreignField:'profesor',
                    as:'elprofe'
                }
            }, 
            {
                $unwind: '$elprofe'
            },
            {
                $project:{
                    nombreprofe:'$nombre',
                    apellidoprofe:'$apellido',
                    correoprofe:'$correo',
                    documentoprofe:'$documento',
                    cursoprofe:'$elprofe.cursos'
                }
            }

        ]
        ).pretty()





db.createCollection("estudiantesCurso")

db.estudiantesCurso.insertMany([
    {
       estudiante: new ObjectId("5fb042314f50bdc3ac48d194"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1a8")
     
    
    },
    {
       estudiante: new ObjectId("5fb042314f50bdc3ac48d195"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1a9")
       
    },
    {
         estudiante: new ObjectId("5fb042314f50bdc3ac48d196"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1aa")
         
    },
        {
         estudiante: new ObjectId("5fb042314f50bdc3ac48d197"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1ab")
         
    },
        {
         estudiante: new ObjectId("5fb042314f50bdc3ac48d198"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1ac")
        
    },
        {
         estudiante: new ObjectId("5fb042314f50bdc3ac48d199"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1ad")
   
    },
        {
         estudiante: new ObjectId("5fb042314f50bdc3ac48d19a"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1ae")
    
    },
        {
         estudiante: new ObjectId("5fb042314f50bdc3ac48d19b"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1af")
         
    },
       {
       estudiante: new ObjectId("5fb042314f50bdc3ac48d19c"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1a8")
     
    
    },
    {
       estudiante: new ObjectId("5fb042314f50bdc3ac48d19d"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1a9")
       
    },
    {
         estudiante: new ObjectId("5fb042314f50bdc3ac48d19e"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1aa")
         
    },
        {
         estudiante: new ObjectId("5fb042314f50bdc3ac48d19f"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1ab")
         
    },
        {
         estudiante: new ObjectId("5fb042314f50bdc3ac48d1a0"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1ac")
        
    },
        {
         estudiante: new ObjectId("5fb042314f50bdc3ac48d1a1"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1ad")
   
    },
        {
         estudiante: new ObjectId("5fb042314f50bdc3ac48d1a2"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1ae")
    
    },
        {
         estudiante: new ObjectId("5fb042314f50bdc3ac48d1a3"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1af")
         
    },

        {
         estudiante: new ObjectId("5fb042314f50bdc3ac48d1a4"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1ac")
        
    },
        {
         estudiante: new ObjectId("5fb042314f50bdc3ac48d1a5"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1ad")
   
    },
        {
         estudiante: new ObjectId("5fb042314f50bdc3ac48d1a6"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1ae")
    
    },
        {
         estudiante: new ObjectId("5fb042314f50bdc3ac48d1a7"),
        cursos: new ObjectId("5fb042ca4f50bdc3ac48d1af")
         
    }
])


    db.estudiantes.aggregate(
        [
            {
                $match:{
                    _id: ObjectId("5fb042314f50bdc3ac48d1a7"),
                }
            },
            {
                $lookup:{
                      from:'estudiantesCurso',
                    localField:'_id',
                    foreignField:'estudiante',
                    as:'elestudiante'
                }
            }, 
            {
                $unwind: '$elestudiante'
            },
            {
                $project:{
                    nombreestu:'$nombre',
                    apellidoestu:'$apellido',
                    correoestu:'$correo',
                    documentoestu:'$documento',
                    cursoestu:'$elestudiante.cursos'
                }
            }

        ]
        ).pretty()
